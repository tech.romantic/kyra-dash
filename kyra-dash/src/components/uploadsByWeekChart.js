import React from "react";
import styled from "styled-components";
import moment from "moment";
import { kyraYellow } from "../styles/colors";
import { groupBy } from "lodash";
import { withLoading } from "../hoc/withLoading";
import {
  LineChart,
  Line,
  Tooltip,
  ResponsiveContainer,
  XAxis,
  YAxis,
  CartesianGrid
} from "recharts";

const UploadsByWeekChart = ({ videos }) => {
  var data = createUploadsByWeekDataFromVideos(videos);
  return (
    <ChartContainer>
      <ChartTitle>Weekly Video Uploads</ChartTitle>
      <ResponsiveContainer width="75%" height="75%" minHeight={0}>
        <LineChart h data={data}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis
            type="category"
            datakey="name"
            tickFormatter={tick => data[tick].name}
          ></XAxis>
          <YAxis allowDecimals={false}></YAxis>
          <Tooltip
            labelFormatter={label => data[label].name}
            formatter={(value, name, props) => [value, "Videos Uploaded"]}
          />
          <Line dataKey="videoCount"></Line>
        </LineChart>
      </ResponsiveContainer>
    </ChartContainer>
  );
};

export default withLoading(UploadsByWeekChart);
const ChartContainer = styled.div`
  grid-row: 1 / 2;
  grid-column: 1 /2;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const ChartTitle = styled.div`
	color: ${kyraYellow}
	font-size: 3vh; 
	
`;

const createUploadsByWeekDataFromVideos = videos => {
  var videosByWeek = groupBy(videos, video => {
    return moment(video.datePublished)
      .startOf("week")
      .format("MM-DD-YYYY");
  });

  var weeklyVideosChartArr = Object.keys(videosByWeek).map(groupedDate => {
    return {
      name: groupedDate,
      videoCount: videosByWeek[groupedDate].length
    };
  });

  return weeklyVideosChartArr;
};
