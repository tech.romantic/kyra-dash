import React from "react";
import styled from "styled-components";
import { kyraYellow, dark } from "../styles/colors";
import moment from "moment";
import { withLoading } from "../hoc/withLoading";

const VideoList = ({ videos }) => {
  return (
    <VideoListRoot>
      <VideoScrollList>{videos.map(renderVideoCard)}</VideoScrollList>
    </VideoListRoot>
  );
};

const VideoListRoot = styled.div`
  display: grid;
  grid-template-rows: 1fr;
  overflow-y: hidden;
  border: solid 1px ${kyraYellow};
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
`;
const VideoScrollList = styled.div`
  overflow-y: scroll;
  grid-row: 1 / 1;
  height: 100%;
  padding: 0.5em;
  overflow-x: hidden;
  max-width: 100%;
`;

const VideoCard = styled.div`
  width: 100%;
  height: 60px;
  margin: 0.5em 0 0.5em 0;
  border: 1px solid ${kyraYellow};
  border-radius: 5px;
  background-color: #292929;
  transition: all 0.3s ease-in-out;
  &:hover {
    box-shadow: 0 10px 20px 10px ${dark};
  }
  display: grid;
  grid-template-columns: 75px 1fr 75px;
`;

const renderVideoCard = video => {
  const videoDate = moment(video.datePublished).toObject();
  return (
    <VideoCard key={video.datePublished.toString()}>
      <VideoDate>
        <VideoDayMonth>
          {videoDate.months + 1 + "/" + (videoDate.date + 1)}
        </VideoDayMonth>
        <VideoYear>{videoDate.years}</VideoYear>
      </VideoDate>
      <VideoTitle>{video.title}</VideoTitle>
      <VideoThumbnail
        src={video.thumbnails ? video.thumbnails.default.url : null}
      ></VideoThumbnail>
    </VideoCard>
  );
};

const VideoTitle = styled.div`
  grid-column: 2 / 3;
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  font-size: 18px;
  padding-left: 0.5em;
  color: white;
  font-family: "Reem Kufi", sans-serif;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`;

const VideoDate = styled.div`
  display: grid;
  grid-template-rows: 1fr 1fr;
  padding: 0.25em;
  color: ${dark};
  background: ${kyraYellow};
  font-family: "Bungee";
`;

const VideoDayMonth = styled.div`
  grid-row: 1 / 2;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;
const VideoYear = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  grid-row: 2 / 3;
  grid-column: 1 / 3;
  font-size: 12px;
`;

const VideoThumbnail = styled.img`
  width: 100%;
  height: 100%;
  padding-right: 0.5em;
  border-radius: 10px;
`;
export default withLoading(VideoList);
