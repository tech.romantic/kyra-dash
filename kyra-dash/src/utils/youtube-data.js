import moment from "moment";

export const loadClientPromisified = async () => {
  return new Promise((resolve, reject) => {
    window.gapi.load("client", () => {
      resolve(true);
    });
  });
};

export const setClientApiKey = async () => {
  return new Promise((resolve, reject) => {
    window.gapi.client.setApiKey(process.env.REACT_APP_YOUTUBE_API_KEY);
    resolve(true);
  });
};

export const loadYoutubeAPI = async () => {
  return new Promise((resolve, reject) => {
    window.gapi.client
      .load("https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest")
      .then(
        () => {
          console.log("GAPI client loaded for API");
          resolve(true);
        },
        err => {
          console.error("Error loading GAPI client for API", err);
          reject(false);
        }
      );
  });
};

export const retrieveVideoPlaylistsForChannel = (
  channelId = "UCvO6uJUVJQ6SrATfsWR5_aA"
) => {
  return new Promise((resolve, reject) => {
    window.gapi.client.youtube.playlists
      .list({
        part: "snippet, contentDetails",
        channelId: channelId,
        maxResults: 50
      })
      .then(
        response => {
          resolve(response.result);
        },
        err => {
          reject(err);
        }
      );
  });
};

export const getAllVideosForPlaylist = async (playlistId, pageToken = "") => {
  const videosSection = await getPageOfVideos(playlistId, pageToken);
  if (videosSection.nextPageToken) {
    return videosSection.items.concat(
      await getAllVideosForPlaylist(playlistId, videosSection.nextPageToken)
    );
  } else {
    return videosSection.items;
  }
};

export const getPageOfVideos = async (playlistId, pageToken) => {
  var videos = await getVideosInPlaylist(playlistId, pageToken);
  var { items, nextPageToken } = videos;
  return {
    items,
    nextPageToken
  };
};

export const getVideosInPlaylist = async (playlistId, pageToken) => {
  return new Promise((resolve, reject) => {
    window.gapi.client.youtube.playlistItems
      .list({
        part: "id, snippet, contentDetails",
        playlistId: playlistId,
        maxResult: 10,
        pageToken: pageToken ? pageToken : ""
      })
      .then(
        async response => {
          resolve(response.result);
        },
        err => {
          reject(err);
        }
      );
  });
};

export const toVideoUIData = video => {
  return {
    title: video.snippet.title,
    id: video.contentDetails.videoId,
    datePublished: moment(video.contentDetails.videoPublishedAt).toString(),
    thumbnails: video.snippet.thumbnails
  };
};
