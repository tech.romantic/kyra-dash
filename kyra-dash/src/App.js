import React from "react";
// Redux
import store from "./redux/store";
import { connect, Provider } from "react-redux";
import { initialLoadThunkCreator, manualChannelSearch } from "./redux/videos";

// Features
import KyraDash from "./features/dashboard";
import "./App.css";

class App extends React.Component {
  constructor(props) {
    super(props);
    props.dispatch(initialLoadThunkCreator());
    // Update every 5 minutes.
    setTimeout(() => {
      props.dispatch(manualChannelSearch());
    }, 300000);
  }

  render() {
    return <KyraDash></KyraDash>;
  }
}

const mapStateToProps = state => state;

const ConnectedKyraDash = connect(mapStateToProps)(App);

const KyraDashWithStore = () => {
  return (
    <Provider store={store}>
      <ConnectedKyraDash></ConnectedKyraDash>
    </Provider>
  );
};

export default KyraDashWithStore;
