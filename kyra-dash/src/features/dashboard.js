import React from "react";
import styled from "styled-components";
import media from "styled-media-query";
import { connect } from "react-redux";
import Channeler from "./channeler";
import VideoDashboard from "./videoDashboard";

const KyraDashboard = ({ loading }) => {
  return (
    <DashboardRoot>
      <DashboardGrid>
        <Channeler>Stuff here</Channeler>
        <Main>
          <VideoDashboard loading={loading}></VideoDashboard>
        </Main>
      </DashboardGrid>
    </DashboardRoot>
  );
};

const mapState = state => state.videos;
export default connect(mapState)(KyraDashboard);

const DashboardRoot = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
`;

const DashboardGrid = styled.div`
  height: 100%;
  width: 100%;
  display: grid;
  ${media.greaterThan("large")`
		grid-template-rows: 1fr minmax(0px,5fr); 
	`}
  ${media.lessThan("large")`
		grid-template-rows: 1fr minmax(0px, 8fr);
	`}
`;

const Main = styled.div`
  grid-row: 2 / 3;
  grid-column: 1 / 1;
  display: grid;
`;
