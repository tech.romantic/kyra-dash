import React from "react";
import styled from "styled-components";
import { compose } from "recompose";
import { connect } from "react-redux";
import media from "styled-media-query";
import { kyraYellow } from "../styles/colors";
import VideoList from "../components/videoList";
import UploadsByWeekChart from "../components/uploadsByWeekChart";

const VideoDashboard = ({ videos, loading }) => {
  // const [ref, { x, y, width }] = useDimensions();
  // console.log(x,y);
  return (
    <DashboardRoot>
      <ScrollContainer>
        <VideoList loading={loading} videos={videos}></VideoList>
      </ScrollContainer>
      <ChartContainer>
        <UploadsByWeekChart
          loading={loading}
          videos={videos}
        ></UploadsByWeekChart>
      </ChartContainer>
    </DashboardRoot>
  );
};

const DashboardRoot = styled.div`
  border: solid thick ${kyraYellow};
  grid-row: 1 / 1;
  grid-column: 1 / 1;
  display: grid;
  ${media.greaterThan("large")`
			grid-template-columns: minmax(0,6fr) minmax(0,4fr); 
			grid-template-rows: minmax(0,1fr); 
			margin: 2em;
	`}
  ${media.lessThan("large")`
			grid-template-columns: minmax(0,1fr); 
			grid-template-rows: minmax(0,4fr) minmax(0,6fr); 
			margin: 1em;
	`}
`;

const ScrollContainer = styled.div`
  position: relative;
  ${media.greaterThan("large")`
		grid-row: 1 / 2; 
		grid-column: 1 / 2;
	`}
`;

const ChartContainer = styled.div`
  height: auto;
  ${media.greaterThan("large")`
		grid-row: 1 / 2; 
		grid-column: 2 / 3;
		padding: 1em;
	`}
`;
const mapState = state => state.videos;
const enhance = compose(connect(mapState, null));
export default enhance(VideoDashboard);
