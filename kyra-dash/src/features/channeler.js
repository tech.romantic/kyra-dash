import React, { useState } from "react";
import styled from "styled-components";
import media from "styled-media-query";
import { connect } from "react-redux";
import { manualChannelSearch } from "../redux/videos";
import kyraLogo from "../assets/kyra-yellow.png";
import { kyraYellow, dark } from "../styles/colors";
import { SearchAlt2 } from "styled-icons/boxicons-solid/SearchAlt2";
const Channeler = ({ dispatch, channel, message }) => {
  const [channelToSearch, setChannel] = useState(channel);
  const onChannelChange = e => {
    setChannel(e.target.value);
  };

  const searchChannelVideos = () => {
    dispatch(manualChannelSearch(channelToSearch));
  };

  return (
    <ChannelerRoot>
      <ChannelerBar onClick={() => searchChannelVideos()}>
        {channelToSearch !== "" ? (
          <DarkSearch></DarkSearch>
        ) : (
          <Logo src={kyraLogo}></Logo>
        )}
      </ChannelerBar>
      <ChannelerInput
        onChange={onChannelChange}
        placeholder="enter channel id..."
      />
      <ChannelerDisplay>{message}</ChannelerDisplay>
    </ChannelerRoot>
  );
};

const mapStateToProps = state => state.videos;

export default connect(mapStateToProps)(Channeler);

const DarkSearch = styled(SearchAlt2)`
  color: ${dark};
  height: 50px;
`;

const ChannelerRoot = styled.div`
  height: 100%;
  width: auto;
  ${media.greaterThan("large")`
			margin: 2em 2em 0 2em;
	`}
  ${media.lessThan("large")`
		margin: 0.5em 0.5em 0 0.5em;
	`}
	display: grid;
  grid-template-columns: 100px 1fr;
  grid-template-rows: 4fr 2fr;
`;

const ChannelerBar = styled.div`
  grid-column: 1 / 2;
  grid-row: 1 / 2;
  background: ${kyraYellow};
  width: 100px;
  min-height: 75px;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 100;
  &:hover {
    box-shadow: inset 0 0 10px ${dark};
  }
  transition: 0.25s ease-in;
`;
const ChannelerDisplay = styled.div`
  grid-column: 1 / 3;
  grid-row: 2 / 3;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  color: ${kyraYellow};
  ${media.greaterThan("large")`
		font-size: 5vh; 
	`}
  ${media.lessThan("large")`
	font-size: 3vh; 
	`}
`;

const Logo = styled.img`
  ${media.greaterThan("large")`
		width: 90px;
		object-fit: contain; 
	`}
  ${media.lessThan("large")`
		width: 75px;
	`}
`;
const ChannelerInput = styled.input`
	grid-column: 2 / 3;
	background: ${dark}
	color: ${kyraYellow}
	border: none;
	border-left: 5px solid ${kyraYellow};
	border-bottom: 5px solid ${kyraYellow};
	border-top: 5px solid ${kyraYellow};
	padding-left: 0.25em;
	${media.greaterThan("large")`
		font-size: 8vh; 
	`}
	${media.lessThan("large")`
		font-size: 3vh;
	`}
	
`;
