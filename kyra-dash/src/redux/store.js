import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import videosReducer from "./videos";

const kyraSync = store => next => action => {
  let nextAction = next(action);
  let newState = store.getState();
  localStorage.setItem("kyra-dash-redux", JSON.stringify(newState));
};

const store = configureStore({
  reducer: {
    videos: videosReducer
  },
  middleware: [...getDefaultMiddleware(), kyraSync]
});

export default store;
