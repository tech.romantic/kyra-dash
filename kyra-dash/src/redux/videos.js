import { createSlice } from "@reduxjs/toolkit";
import store from "./store";
import {
  loadClientPromisified,
  loadYoutubeAPI,
  retrieveVideoPlaylistsForChannel,
  setClientApiKey,
  getAllVideosForPlaylist,
  toVideoUIData
} from "../utils/youtube-data";

export const videosSlice = createSlice({
  name: "videos",
  initialState: {
    channel: "",
    message: "",
    videos: [],
    videoCount: 0,
    loading: true
  },
  reducers: {
    setChannelId: (state, action) => {
      state.channel = action.payload;
    },
    setVideos: (state, action) => {
      state.videos = action.payload;
    },
    startLoading: (state, action) => {
      state.loading = true;
    },
    setMessage: (state, action) => {
      state.message = action.payload;
    },
    endLoading: (state, action) => {
      state.loading = false;
    }
  }
});

const { actions, reducer } = videosSlice;
const {
  setVideos,
  setChannelId,
  startLoading,
  endLoading,
  setMessage
} = actions;
export { setVideos, setChannelId, startLoading, endLoading, setMessage };
export default reducer;

export const manualChannelSearch = channel => {
  return async function findVideosForChannel(dispatch) {
    try {
      store.dispatch(startLoading());
      var playlists = await retrieveVideoPlaylistsForChannel(channel);
      var channelName = retrieveChannelUsernameFromPlaylists(playlists);
    } catch (err) {
      store.dispatch(setMessage(err.result.error.message));
      store.dispatch(endLoading());
      return;
    }
    if (playlists.items.length > 0) {
      var recentUploadsPlaylist = playlists.items.find(item =>
        item.snippet.title.includes("Uploads")
      );
      // If we can't find a generic playlist, pick the first...
      if (!recentUploadsPlaylist) recentUploadsPlaylist = playlists.items[0];
      await loadVideos(recentUploadsPlaylist.id, channelName);
    } else {
      store.dispatch(setMessage("No playlists found..."));
    }
  };
};

export const initialLoadThunkCreator = () => {
  return async function loadClientAndCheckVideos(dispatch) {
    // load from storage
    var loadedState = JSON.parse(localStorage.getItem("kyra-dash-redux"));
    // videos redux slice / videos array (videos.videos)
    var previousVideos = loadedState != null ? loadedState.videos.videos : [];
    await loadClientPromisified();
    await setClientApiKey();
    await loadYoutubeAPI();
    var playlists = await retrieveVideoPlaylistsForChannel();
    var channelName = retrieveChannelUsernameFromPlaylists(playlists);
    var recentUploadsPlaylist = playlists.items.find(
      item => item.snippet.title === "Recent Uploads"
    );
    // Use the playlist to detect if new videos have been uploaded since our last save.
    if (
      recentUploadsPlaylist.contentDetails.itemCount > previousVideos.length
    ) {
      await loadVideos(recentUploadsPlaylist.id, channelName);
    } else {
      // Use the previously saved videos
      store.dispatch(setMessage(loadedState.videos.message));
      store.dispatch(setVideos(previousVideos));
      store.dispatch(endLoading());
    }
  };
};

const retrieveChannelUsernameFromPlaylists = playlists => {
  return playlists.items[0].snippet.channelTitle;
};

const loadVideos = async (playlistId, channelName) => {
  var allVideosForPlaylist = await getAllVideosForPlaylist(playlistId);
  var videoData = allVideosForPlaylist.map(toVideoUIData);
  store.dispatch(setMessage("Videos for Channel:" + channelName));
  store.dispatch(setVideos(videoData));
  store.dispatch(endLoading());
};
