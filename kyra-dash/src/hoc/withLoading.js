import React from "react";
import styled, { css } from "styled-components";
import { kyraYellow } from "../styles/colors";
import MoonLoader from "react-spinners/MoonLoader";

const override = css`
	border-color: ${kyraYellow}
	background: ${kyraYellow}
	color: ${kyraYellow}
`;

export const withLoading = Component => {
  return ({ loading, ...props }) => {
    return loading ? <Loader /> : <Component {...props} />;
  };
};

const Loader = () => (
  <CenterLoaderContainer>
    <MoonLoader css={override} size={75}></MoonLoader>
  </CenterLoaderContainer>
);

const CenterLoaderContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
